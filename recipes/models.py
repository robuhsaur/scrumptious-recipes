from django.db import models


# Create your models here.


class Recipe(models.Model):
    name = models.CharField(max_length=50)
    author = models.CharField(max_length=100)
    description = models.CharField(max_length=50)
    image = models.URLField(null=True, blank=True)
    created = models.DateTimeField(auto_now=True)
    updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name + " by " + self.author


class Measure(models.Model):
    name = models.CharField(max_length=30, unique=True)
    abbreviation = models.CharField(max_length=10, unique=True)


class FoodItem(models.Model):
    name = models.CharField(max_length=100)
