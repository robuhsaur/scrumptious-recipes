from django.urls import path

from recipes.views import (
    create_recipe,
    change_recipe,
    show_recipe,
    show_recipes,
    party,
    show_cookbook,
    show_ingredients,
    show_about,
)

urlpatterns = [
    path("", show_recipes, name="recipes_list"),
    path("<int:pk>/", show_recipe, name="recipe_detail"),
    path("new/", create_recipe, name="recipe_new"),
    path("edit/", change_recipe, name="recipe_edit"),
    path("party/", party, name="party_page"),
    path("cookbook/", show_cookbook, name="cookbook_page"),
    path("ingredients/", show_ingredients, name="ingredients_page"),
    path("about/", show_about, name="about_page"),
]
