from django.shortcuts import redirect, render

try:
    from recipes.forms import RecipeForm
    from recipes.models import Recipe
except Exception:
    RecipeForm = None
    Recipe = None


def create_recipe(request):
    if request.method == "POST" and RecipeForm:
        form = RecipeForm(request.POST)
        if form.is_valid():
            recipe = form.save()
            return redirect("recipe_detail", pk=recipe.pk)
    elif RecipeForm:
        form = RecipeForm()
    else:
        form = None
    context = {
        "form": form,
    }
    return render(request, "recipes/new.html", context)


def change_recipe(request, pk):
    if Recipe and RecipeForm:
        instance = Recipe.objects.get(pk=pk)
        if request.method == "POST":
            form = RecipeForm(request.POST, instance=instance)
            if form.is_valid():
                form.save()
                return redirect("recipe_detail", pk=pk)
        else:
            form = RecipeForm(instance=instance)
    else:
        form = None
    context = {
        "form": form,
    }
    return render(request, "recipes/edit.html", context)


def show_recipes(request):
    context = {
        "recipes": Recipe.objects.all()
        if Recipe
        else [{"name": "GOOD FOOOOOOD"}],
    }
    return render(request, "recipes/list.html", context)


def show_recipe(request, pk):
    context = {
        "recipe": Recipe.objects.get(pk=pk) if Recipe else None,
    }
    return render(request, "recipes/detail.html", context)


def party(request):
    print("this is my party function in views")
    return render(request, "recipes/party.html")


def show_cookbook(request):
    print("this is my cookbook page")
    return render(request, "recipes/cookbook.html")


def show_ingredients(request):
    print("this is my ingredients page")
    return render(request, "recipes/ingredients.html")


def show_about(request):
    print("this is the about page")
    return render(request, "recipes/about.html")


# Step 1: Identify the proper route  (make a url path) urls.py
# Step 2: Create the UI (make .html file in templates folder)
# Step 3: Write the view function (make a function for the new page) views.py
# Step 4: respond to the request (in the return statement) views.py
